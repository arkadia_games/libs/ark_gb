#!/usr/bin/env bash
SCRIPT_PATH=$(dirname "$(realpath $0)");
GBDK_HOME=$(realpath "$SCRIPT_PATH/..");

$GBDK_HOME/bin/sdldgb                                                          \
   -n -i -g                                                                    \
   _shadow_OAM=0xC000                                                          \
   -g .STACK=0xE000                                                            \
   -g .refresh_OAM=0xFF80                                                      \
   -b _DATA=0xc0a0                                                             \
   -b _CODE=0x0200                                                             \
   -k $GBDK_HOME/lib/small/asxxxx/gbz80/                                       \
   -l gbz80.lib                                                                \
   -k $GBDK_HOME/lib/small/asxxxx/gb/                                          \
   -l                                                                          \
        gb.lib                                                                 \
        mines.ihx                                                              \
        $GBDK_HOME/lib/small/asxxxx/gb/crt0.o                                  \
        $(find . -iname "*.o")

$GBDK_HOME/bin/ihxcheck mines.ihx
$GBDK_HOME/bin/makebin -Z mines.ihx mines.gb


echo "------------>" $PWD
# rm /tmp/lcc50261.adb mines.ihx /tmp/lcc50260.o /tmp/lcc50260.asm /tmp/lcc50260.lst /tmp/lcc50260.sym /tmp/lcc50260.adb /tmp/lcc50261.o /tmp/lcc50261.asm /tmp/lcc50261.lst /tmp/lcc50261.sym
